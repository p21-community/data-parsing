/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.parsing.excel;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.junit.Assume.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * @author Tim Stone
 */
public class ExcelParserTest {
    @Test
    public void requiredSheetNotPresentFails() throws IOException {
        ExcelParser<Object> parser = ExcelParserBuilder
            .newBuilder()
            .addRequired("Missing Sheet", new FakeSheetParser())
            .build(this.getClass().getResourceAsStream("Test.xlsx"));
        ExcelParser.VerificationResult result = parser.verify();

        assertFalse(result.isValid());
        assertThat(result.getErrors(),
            hasItem(allOf(containsString("Missing Sheet"), containsString("Required"))));
    }

    @Test
    public void requiredSheetPresentPasses() throws IOException {
        ExcelParser<Object> parser = ExcelParserBuilder
            .newBuilder()
            .addRequired("Normal Sheet", new FakeSheetParser())
            .build(this.getClass().getResourceAsStream("Test.xlsx"));
        ExcelParser.VerificationResult result = parser.verify();

        assertTrue(result.isValid());
    }

    @Test
    public void requiredSheetWithEmptyHeaderFails() throws IOException {
        ExcelParser<Object> parser = ExcelParserBuilder
            .newBuilder()
            .addRequired("Empty Sheet", new FakeSheetParser())
            .build(this.getClass().getResourceAsStream("Test.xlsx"));
        ExcelParser.VerificationResult result = parser.verify();

        assertFalse(result.isValid());
        assertThat(result.getErrors(),
            hasItem(allOf(containsString("Empty Sheet"), containsString("header"))));
    }

    @Test
    public void sheetWithRequiredColumnNotPresentFails() throws IOException {
        ExcelParser<Object> parser = ExcelParserBuilder
            .newBuilder()
            .addRequired("Normal Sheet", new FakeSheetParser().addRequiredColumn("Fake"))
            .build(this.getClass().getResourceAsStream("Test.xlsx"));
        ExcelParser.VerificationResult result = parser.verify();

        assertFalse(result.isValid());
        assertThat(result.getErrors(),
            hasItem(allOf(containsString("fake"), containsString("missing"))));
    }

    @Test
    public void sheetWithRequiredColumnPresentPasses() throws IOException {
        ExcelParser<Object> parser = ExcelParserBuilder
            .newBuilder()
            .addRequired("Normal Sheet", new FakeSheetParser().addRequiredColumn("A"))
            .build(this.getClass().getResourceAsStream("Test.xlsx"));
        ExcelParser.VerificationResult result = parser.verify();

        assertTrue(result.isValid());
    }

    @Test
    public void columnsAreMappedCorrectly() throws IOException {
        ExcelParser<Object> parser = ExcelParserBuilder
            .newBuilder()
            .addRequired("Normal Sheet", new BaseExcelSheetParser<Object>() {
                @Override
                public void parse(NamedRow row, Object state) {
                    assertTrue(row.has("B"));
                    assertEquals("Test", row.get("B"));
                }
            }.addRequiredColumn("B")).build(this.getClass().getResourceAsStream("Test.xlsx"));
        ExcelParser.VerificationResult result = parser.verify();

        assumeTrue(result.isValid());

        parser.parse(new BaseStateBuilder<Object>() {
            @Override
            public Object build() {
                return new Object();
            }
        });
    }

    @Test
    public void numericColumnsDontMagicallyConvertToScientificNotation() throws IOException {
        ExcelParser<Object> parser = ExcelParserBuilder
            .newBuilder()
            .addRequired("Normal Sheet", new BaseExcelSheetParser<Object>() {
                @Override
                public void parse(NamedRow row, Object state) {
                    assumeTrue(row.has("A"));

                    assertEquals("0.0000032", row.get("A"));
                }
            }.addRequiredColumn("A")).build(this.getClass().getResourceAsStream("Test.xlsx"));
        ExcelParser.VerificationResult result = parser.verify();

        assumeTrue(result.isValid());

        parser.parse(new BaseStateBuilder<Object>() {
            @Override
            public Object build() {
                return new Object();
            }
        });
    }

    @Test
    public void missingOptionalSheetDoesNotFailParsing() throws IOException {
        ExcelParser<Object> parser = ExcelParserBuilder
                .newBuilder()
                .addRequired("Normal Sheet", new BaseExcelSheetParser<Object>() {
                    @Override
                    public void parse(NamedRow row, Object state) {
                        assumeTrue(row.has("A"));
                    }
                }
                .addRequiredColumn("A"))
                .addOptional("Non Existent Sheet", new BaseExcelSheetParser<Object>() {
                    @Override
                    public void parse(NamedRow row, Object state) {

                    }
                }).build(this.getClass().getResourceAsStream("Test.xlsx"));
        ExcelParser.VerificationResult result = parser.verify();

        assumeTrue(result.isValid());

        parser.parse(new BaseStateBuilder<Object>() {
            @Override
            public Object build() {
                return new Object();
            }
        });
    }

    private static class FakeSheetParser extends BaseExcelSheetParser<Object> {
        @Override
        public void parse(NamedRow row, Object state) {}
    }
}
