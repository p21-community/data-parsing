/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.parsing.excel;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Tim Stone
 */
public abstract class BaseExcelSheetParser<T> implements ExcelSheetParser<T> {
    private final Set<String> columns = new HashSet<>();
    private final Set<String> requiredColumns = new HashSet<>();

    public BaseExcelSheetParser<T> addColumn(String name) {
        this.columns.add(name);

        return this;
    }

    public BaseExcelSheetParser<T> addColumns(String...names) {
        for (String name : names) {
            this.addColumn(name);
        }

        return this;
    }

    public BaseExcelSheetParser<T> addRequiredColumn(String name) {
        this.addColumn(name);
        this.requiredColumns.add(name);

        return this;
    }

    public BaseExcelSheetParser<T> addRequiredColumns(String...names) {
        for (String name : names) {
            this.addRequiredColumn(name);
        }

        return this;
    }

    @Override
    public Set<String> getColumns() {
        return Collections.unmodifiableSet(this.columns);
    }

    @Override
    public Set<String> getRequiredColumns() {
        return Collections.unmodifiableSet(this.requiredColumns);
    }

    @Override
    public abstract void parse(NamedRow row, T state);

    @Override
    public void complete(T state) {}
}
