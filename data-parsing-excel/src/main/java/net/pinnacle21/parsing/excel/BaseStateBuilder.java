/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.parsing.excel;

import java.util.*;

/**
 * @author Tim Stone
 */
public abstract class BaseStateBuilder<T> implements StateBuilder<T> {
    protected final List<AttributeDefinition> expectedAttributes;
    protected final Map<String, String> attributes = new HashMap<>();

    protected BaseStateBuilder() {
        this(new AttributeDefinition[0]);
    }

    protected BaseStateBuilder(AttributeDefinition...attributes) {
        this.expectedAttributes = Arrays.asList(attributes);
    }

    @Override
    public void setAttribute(String property, String value) {
        this.attributes.put(property.toLowerCase(), value);
    }

    protected Set<AttributeDefinition> getMissingAttributes() {
        Set<AttributeDefinition> missingAttributes = new HashSet<>();

        for (AttributeDefinition expectedAttribute : this.expectedAttributes) {
            if (!this.attributes.containsKey(expectedAttribute.getKey())) {
                missingAttributes.add(expectedAttribute);
            }
        }

        return missingAttributes;
    }

    protected static AttributeDefinition optionalAttr(String name) {
        return new AttributeDefinition(name, false);
    }

    protected static AttributeDefinition requiredAttr(String name) {
        return new AttributeDefinition(name, true);
    }

    protected static final class AttributeDefinition {
        private final String name;
        private final boolean isRequired;

        protected AttributeDefinition(String name, boolean isRequired) {
            this.name = name;
            this.isRequired = isRequired;
        }

        protected String getName() {
            return this.name;
        }

        protected String getKey() {
            return this.name.toLowerCase();
        }

        protected boolean isRequired() {
            return this.isRequired;
        }

        @Override
        public int hashCode() {
            return this.name.hashCode();
        }

        @Override
        public boolean equals(Object o) {
            return o instanceof AttributeDefinition && ((AttributeDefinition)o).name.equals(this.name);
        }
    }
}
