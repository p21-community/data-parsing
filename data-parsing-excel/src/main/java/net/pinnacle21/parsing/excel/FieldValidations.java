/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.parsing.excel;

import java.util.Set;

public class FieldValidations {
    public static final String REQUIRED_COLUMN_ERROR = "'%s' must be populated, but it is empty";
    public static final String EXCESS_LENGTH_ERROR = "'%s' defines a value that is over %d characters long, "
            + "please shorten it";
    public static final String INVALID_VALUE_ERROR = "'%s' can only contain values that match one of: %s";
    public static final int DEFAULT_LENGTH = 255;
    public static final int MEDIUM_LENGTH = 1000;
    public static final int LONG_LENGTH = 4000;
    public static final int REALLY_LONG_LENGTH = 10000;
    public static final int UNLIMITED_LENGTH = -1;

    private FieldValidations() {}

    public static boolean checkRequired(RowIdentifier identifier, String column, String value,
            ValidatedExcelParse validation) {
        if (isBlank(value)) {
            validation.error(identifier, REQUIRED_COLUMN_ERROR, column);

            return false;
        }

        return true;
    }

    public static boolean checkMaxlength(RowIdentifier identifier, String column, String value, int maxlength,
            ValidatedExcelParse validation) {
        if (!isBlank(value) && value.length() > maxlength && maxlength != UNLIMITED_LENGTH) {
            validation.error(identifier, EXCESS_LENGTH_ERROR, column, maxlength);

            return false;
        }

        return true;
    }

    public static boolean checkAllowed(RowIdentifier identifier, String column, String value, Set<String> values,
            ValidatedExcelParse validation) {
        if (!isBlank(value) && !values.contains(value)) {
            validation.error(identifier, INVALID_VALUE_ERROR, column, String.join(", ", values));

            return false;
        }

        return true;
    }

    private static boolean isBlank(String value) {
        return value == null || value.trim().isEmpty();
    }
}
