/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.parsing.excel;

import org.apache.poi.ss.usermodel.Row;

/**
 * @author Tim Stone
 */
public class ExcelNamedRow implements NamedRow {
    private final ExcelColumnMapping mapping;
    private final Row row;
    private final RowIdentifier identifier;

    ExcelNamedRow(Row row, ExcelColumnMapping mapping) {
        this.row = row;
        this.mapping = mapping;
        this.identifier = new RowIdentifier(row.getSheet().getSheetName(), row.getRowNum() + 1);
    }

    @Override
    public boolean has(String name) {
        return this.mapping.has(name);
    }

    @Override
    public String get(String name) {
        return this.mapping.get(this.row, name);
    }

    @Override
    public RowIdentifier getIdentifier() {
        return this.identifier;
    }
}
