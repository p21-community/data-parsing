/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.parsing.excel;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class ValidationAwareExcelSheetParser<T> extends BaseExcelSheetParser<T> {
    private final Set<String> requiredColumns = new HashSet<>();
    private final Map<String, Integer> maxlengths = new HashMap<>();

    public ValidationAwareExcelSheetParser<T> addValidatedRequiredColumn(String name) {
        return addValidatedRequiredColumn(name, FieldValidations.DEFAULT_LENGTH);
    }

    public ValidationAwareExcelSheetParser<T> addValidatedRequiredColumn(String name, int maxlength) {
        this.addRequiredColumn(name);
        this.addValidatedColumn(name, maxlength);
        this.requiredColumns.add(name);

        return this;
    }

    public ValidationAwareExcelSheetParser<T> addValidatedColumn(String name) {
        return this.addValidatedColumn(name, FieldValidations.DEFAULT_LENGTH);
    }

    public ValidationAwareExcelSheetParser<T> addValidatedColumn(String name, int maxlength) {
        this.addColumn(name);
        this.maxlengths.put(name, maxlength);

        return this;
    }

    protected boolean validate(NamedRow row, ValidatedExcelParse validation) {
        boolean result = true;

        for (String requiredColumn : this.requiredColumns) {
            if (!FieldValidations.checkRequired(row.getIdentifier(), requiredColumn, row.get(requiredColumn),
                    validation)) {
                result = false;
            }
        }

        for (Map.Entry<String, Integer> maxlength : this.maxlengths.entrySet()) {
            String column = maxlength.getKey(),
                    value = row.get(column);

            if (!FieldValidations.checkMaxlength(row.getIdentifier(), column, value, maxlength.getValue(),
                    validation)) {
                result = false;
            }
        }

        return result;
    }
}
