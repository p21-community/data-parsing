/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.parsing.excel;

/**
 * @author Tim Stone
 */
public class RowIdentifier implements Comparable<RowIdentifier> {
    private final String sheet;
    private final Integer row;

    public RowIdentifier(String sheet, Integer row) {
        this.sheet = sheet;
        this.row = row;
    }

    public String getSheet() {
        return this.sheet;
    }

    public Integer getRow() {
        return this.row;
    }

    @Override
    public String toString() {
        return this.getSheet() + ", " + this.getRow();
    }

    @Override
    public int compareTo(RowIdentifier identifier) {
        if (this.getSheet().equals(identifier.getSheet())) {
            throw new UnsupportedOperationException(String.format(
                "Can't compare RowIdentifiers from different sheets (%s vs %s)", this.getSheet(), identifier.getSheet()
            ));
        }

        return this.row.compareTo(identifier.row);
    }

    @Override
    public boolean equals(Object o) {
        return o != null && o instanceof RowIdentifier && this.compareTo((RowIdentifier)o) == 0;
    }
}
