/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.parsing.excel;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

/**
 * @author Tim Stone
 */
public final class ExcelParserBuilder<T> {
    private final LinkedHashMap<String, ExcelSheetParser<T>> sheets = new LinkedHashMap<>();
    private final Set<String> requiredSheets = new HashSet<>();
    private final ExcelParser.InitializingSheetParser<T> initializingSheet;

    private ExcelParserBuilder(ExcelParser.InitializingSheetParser<T> initializingSheet) {
        this.initializingSheet = initializingSheet;
    }

    public static <T> ExcelParserBuilder<T> newBuilder() {
        return new ExcelParserBuilder<>(null);
    }

    public static <T> ExcelParserBuilder<T> newBuilder(String initializingSheet) {
        return new ExcelParserBuilder<>(new ExcelParser.InitializingSheetParser<>(
            initializingSheet, new AttributeSheetParser<T>()
        ));
    }

    public ExcelParserBuilder<T> addOptional(String name, ExcelSheetParser<T> parser) {
        this.sheets.put(name, parser);

        return this;
    }

    public ExcelParserBuilder<T> addRequired(String name, ExcelSheetParser<T> parser) {
        this.addOptional(name, parser);
        this.requiredSheets.add(name);

        return this;
    }

    /**
     * Creates a parser from a file. This method should be preferred over using an <code>InputStream</code>
     * due to <a href="http://poi.apache.org/spreadsheet/quick-guide.html#FileInputStream">memory considerations</a>
     *
     * @param source  the source file for the Excel document
     * @return  the configured parser
     * @throws IOException  if the source can't be read
     */
    public ExcelParser<T> build(File source) throws IOException {
        return new ExcelParser<>(null, source, this.initializingSheet, this.sheets, this.requiredSheets);
    }

    /**
     * Creates a parser from an input stream
     *
     * @deprecated  prefer {@link #build(File)}
     * @param source  the source file for the Excel document
     * @return  the configured parser
     * @throws IOException  if the source can't be read
     */
    public ExcelParser<T> build(InputStream source) throws IOException {
        return new ExcelParser<>(source, null, this.initializingSheet, this.sheets, this.requiredSheets);
    }

    private static class AttributeSheetParser<T> extends BaseExcelSheetParser<StateBuilder<T>> {
        private static final String ATTRIBUTE_COLUMN = "Attribute";
        private static final String VALUE_COLUMN = "Value";

        AttributeSheetParser() {
            this.addRequiredColumn(ATTRIBUTE_COLUMN);
            this.addRequiredColumn(VALUE_COLUMN);
        }

        @Override
        public void parse(NamedRow row, StateBuilder<T> state) {
            if (row.has(ATTRIBUTE_COLUMN)) {
                state.setAttribute(row.get(ATTRIBUTE_COLUMN), row.get(VALUE_COLUMN));
            }
        }
    }
}
