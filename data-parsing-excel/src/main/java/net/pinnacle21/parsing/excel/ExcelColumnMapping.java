/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.parsing.excel;

import org.apache.poi.ss.formula.FormulaParseException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author Tim Stone
 */
public class ExcelColumnMapping {
    private static final ThreadLocal<DecimalFormat> DECIMAL_PATTERN = new ThreadLocal<DecimalFormat>() {
        @Override
        protected DecimalFormat initialValue() {
            DecimalFormat format = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));

            format.setMaximumFractionDigits(10);

            return format;
        }
    };

    private final Map<String, Integer> columnIndexes = new HashMap<>();
    private final FormulaEvaluator evaluator;

    ExcelColumnMapping(FormulaEvaluator evaluator) {
        this.evaluator = evaluator;
    }

    void set(String name, int index) {
        this.columnIndexes.put(name, index);
    }

    String get(Row row, String name) {
        return get(row, this.columnIndexes.get(name), this.evaluator);
    }

    boolean has(String name) {
        return this.columnIndexes.containsKey(name);
    }

    static String get(Row row, Integer index, FormulaEvaluator evaluator) {
        if (index == null) {
            return "";
        }

        Cell cell = row.getCell(index, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
        CellValue evaluatedCell = null;

        if (cell.getCellType() == Cell.CELL_TYPE_FORMULA) {
            try {
                evaluatedCell = evaluator.evaluate(cell);
            } catch (FormulaParseException e) {
                return cell.getCellFormula();
            }
        }

        if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC
                || (evaluatedCell != null && evaluatedCell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
            double numericValue = evaluatedCell != null
                ? evaluatedCell.getNumberValue()
                : cell.getNumericCellValue();

            return DECIMAL_PATTERN.get().format(numericValue);
        } else if (cell.getCellType() == Cell.CELL_TYPE_ERROR
                || (evaluatedCell != null && evaluatedCell.getCellType() == Cell.CELL_TYPE_ERROR)) {
            return "ERROR";
        }

        return evaluatedCell != null
            ? evaluatedCell.getStringValue()
            : cell.getStringCellValue();
    }
}
