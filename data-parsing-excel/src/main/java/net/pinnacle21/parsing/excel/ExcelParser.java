/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.parsing.excel;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * @author Tim Stone
 */
public class ExcelParser<T> {
    private final InitializingSheetParser<T> initializingSheet;
    private final LinkedHashMap<String, ExcelSheetParser<T>> sheets;
    private final Set<String> requiredSheets;
    private final Workbook workbook;
    private final FormulaEvaluator evaluator;
    private final Map<String, SheetMetadata> metadata = new HashMap<>();

    ExcelParser(InputStream sourceStream, File sourceFile, InitializingSheetParser<T> initializingSheet,
            LinkedHashMap<String, ExcelSheetParser<T>> sheets, Set<String> requiredSheets) throws IOException {
        this.initializingSheet = initializingSheet;
        this.sheets = sheets;
        this.requiredSheets = new HashSet<>();

        if (this.initializingSheet != null) {
            this.requiredSheets.add(this.initializingSheet.getName());
        }

        this.requiredSheets.addAll(requiredSheets);

        try {
            if (sourceFile != null) {
                this.workbook = WorkbookFactory.create(sourceFile);
            } else {
                this.workbook = WorkbookFactory.create(sourceStream);
            }
        } catch (InvalidFormatException ex) {
            // TODO: Decide if this is the best way to handle this
            throw new IllegalArgumentException("The provided source is not a valid XSLX document", ex);
        }

        this.evaluator = this.workbook.getCreationHelper().createFormulaEvaluator();

        for (int i = 0; i < this.workbook.getNumberOfSheets(); i++) {
            // trim prevents hidden spaces at end of sheet name from breaking import, since we use the index instead
            // of sheetname for retrieving sheets we don't need to worry that the sheetname is different in excel file
            this.metadata.put(this.workbook.getSheetName(i).trim().toLowerCase(),
                new SheetMetadata(i, new ExcelColumnMapping(this.evaluator)));
        }
    }

    public VerificationResult verify() {
        VerificationResult result = new VerificationResult();

        Set<String> presentSheetNames = this.metadata.keySet();

        for (String sheetName : this.requiredSheets) {
            if (!presentSheetNames.contains(sheetName.toLowerCase())) {
                result.raise("Required sheet %s is missing from the document", sheetName);
            }
        }

        for (String sheetName : this.getSheetNames()) {
            SheetMetadata metadata = this.metadata.get(sheetName.toLowerCase());

            if (metadata != null) {
                Sheet sheet = this.workbook.getSheetAt(metadata.getIndex());
                // TODO: Is this a safe assumption that the first record is at 0?
                Row header = sheet.getRow(0);

                if (header != null) {
                    Map<String, String> expectedColumnNamesMapping = new HashMap<>();
                    Set<String> presentColumnNames = new HashSet<>();
                    Set<String> requiredColumnNames = new HashSet<>();

                    for (String columnName : this.getSheet(sheetName).getColumns()) {
                        expectedColumnNamesMapping.put(columnName.toLowerCase(), columnName);
                    }

                    Set<String> expectedColumnNames = expectedColumnNamesMapping.keySet();

                    for (String columnName : this.getSheet(sheetName).getRequiredColumns()) {
                        requiredColumnNames.add(columnName.toLowerCase());
                    }

                    for (int i = 0; i < header.getLastCellNum(); ++i) {
                        String columnName = ExcelColumnMapping.get(header, i, this.evaluator).toLowerCase();

                        if (!columnName.isEmpty()) {
                            presentColumnNames.add(columnName);

                            if (expectedColumnNames.contains(columnName)) {
                                metadata.getColumnMapping().set(expectedColumnNamesMapping.get(columnName), i);
                            }
                        }
                    }

                    for (String columnName : requiredColumnNames) {
                        if (!presentColumnNames.contains(columnName)) {
                            // TODO: The column name is converted to lowercase which I guess isn't desirable
                            result.raise("Required column %s is missing from sheet %s", columnName, sheetName);
                        }
                    }
                } else if (this.requiredSheets.contains(sheetName)) {
                    result.raise("Sheet %s does not have a header record", sheetName);
                }
            }
        }

        return result;
    }

    public T parse(StateBuilder<T> stateBuilder) {
        return this.parse(stateBuilder, null);
    }

    public T parse(StateBuilder<T> stateBuilder, ParsingMonitor monitor) {
        if (monitor == null) {
            monitor = new BlankParsingMonitor();
        }

        monitor.onStart();

        if (this.initializingSheet != null) {
            this.parse(this.initializingSheet.getName(), this.initializingSheet.getParser(), stateBuilder, monitor);
        }

        T state = stateBuilder.build();

        for (Map.Entry<String, ExcelSheetParser<T>> entry : this.sheets.entrySet()) {
            this.parse(entry.getKey(), entry.getValue(), state, monitor);
        }

        monitor.onComplete();

        return state;
    }

    private <U> void parse(String sheetName, ExcelSheetParser<U> parser, U state, ParsingMonitor monitor) {
        monitor.onParse(sheetName);

        if (!this.metadata.containsKey(sheetName.toLowerCase()) && !this.requiredSheets.contains(sheetName)) {
            parser.complete(state);
            return;
        }

        SheetMetadata metadata = this.metadata.get(sheetName.toLowerCase());

        boolean isHeader = true;

        for (Row row : this.workbook.getSheetAt(metadata.getIndex())) {
            boolean rowActuallyHasData = false;
            for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {
                String cellValue = ExcelColumnMapping.get(row, i, this.evaluator);
                if (cellValue != null && cellValue.trim().length() > 0) {
                    rowActuallyHasData = true;
                    break;
                }
            }

            if (rowActuallyHasData && (!isHeader || (isHeader = false))) {
                parser.parse(new ExcelNamedRow(row, metadata.getColumnMapping()), state);
            }
        }

        parser.complete(state);
    }

    private Set<String> getSheetNames() {
        Set<String> names = new HashSet<>(this.sheets.keySet());

        if (this.initializingSheet != null) {
            names.add(this.initializingSheet.getName());
        }

        return names;
    }

    private ExcelSheetParser<?> getSheet(String name) {
        ExcelSheetParser<?> sheet = this.sheets.get(name);

        if (sheet == null && this.initializingSheet != null && this.initializingSheet.getName().equals(name)) {
            sheet = this.initializingSheet.getParser();
        }

        return sheet;
    }

    public static class VerificationResult {
        private final List<String> errors = new LinkedList<>();

        private VerificationResult() {}

        private void raise(String error, Object...variables) {
            this.errors.add(String.format(error, variables));
        }

        public boolean isValid() {
            return this.errors.isEmpty();
        }

        public List<String> getErrors() {
            return Collections.unmodifiableList(this.errors);
        }
    }

    static class InitializingSheetParser<T> {
        private final String name;
        private final ExcelSheetParser<StateBuilder<T>> parser;

        InitializingSheetParser(String name, ExcelSheetParser<StateBuilder<T>> parser) {
            this.name = name;
            this.parser = parser;
        }

        String getName() {
            return this.name;
        }

        ExcelSheetParser<StateBuilder<T>> getParser() {
            return this.parser;
        }
    }

    private static class SheetMetadata {
        private final int index;
        private final ExcelColumnMapping mapping;

        private SheetMetadata(int index, ExcelColumnMapping mapping) {
            this.index = index;
            this.mapping = mapping;
        }

        public int getIndex() {
            return this.index;
        }

        public ExcelColumnMapping getColumnMapping() {
            return this.mapping;
        }
    }
}
