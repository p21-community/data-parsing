/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.parsing.excel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BaseValidatedExcelParse implements ValidatedExcelParse {
    private final List<String> errors = new ArrayList<>();
    private final List<String> warnings = new ArrayList<>();

    @Override
    public void error(RowIdentifier identifier, String message, Object... args) {
        this.errors.add((identifier == null ? "" : identifier + " ") + String.format(message, args));
    }

    @Override
    public void warn(RowIdentifier identifier, String message, Object... args) {
        this.warnings.add((identifier == null ? "" : identifier + " ") + String.format(message, args));
    }

    @Override
    public List<String> getErrors() {
        return Collections.unmodifiableList(this.errors);
    }

    @Override
    public List<String> getWarnings() {
        return Collections.unmodifiableList(this.warnings);
    }

    @Override
    public boolean isValid() {
        return this.errors.isEmpty() && this.warnings.isEmpty();
    }
}
