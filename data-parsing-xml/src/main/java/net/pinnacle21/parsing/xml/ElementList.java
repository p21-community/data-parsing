/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.parsing.xml;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.Iterator;

/**
 * @author Tim Stone
 */
public class ElementList implements Iterable<Element> {
    private final NodeList nodes;

    public ElementList(NodeList nodes) {
        this.nodes = nodes;
    }

    public boolean hasElements() {
        return this.nodes.getLength() > 0;
    }

    public Element get(int index) {
        if (index < 0 || index > this.nodes.getLength()) {
            throw new ArrayIndexOutOfBoundsException();
        }

        return (Element)this.nodes.item(index);
    }

    public int size() {
        return this.nodes.getLength();
    }

    @Override
    public Iterator<Element> iterator() {
        return new XmlUtils.ElementIterator(this.nodes);
    }
}
