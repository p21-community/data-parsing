/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.parsing.xml;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.validation.Schema;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Tim Stone
 */
@SuppressWarnings("unused") // api
public class XmlUtils {
    private static DocumentBuilderFactory SAFE_FACTORY;

    public static DocumentBuilder newSafeDocumentBuilder() throws ParserConfigurationException {
        synchronized (XmlUtils.class) {
            if (SAFE_FACTORY == null) {
                SAFE_FACTORY = newSafeDocumentBuilderFactory();
            }
        }

        return SAFE_FACTORY.newDocumentBuilder();
    }

    public static DocumentBuilder newSafeDocumentBuilder(Schema schema) throws ParserConfigurationException {
        DocumentBuilderFactory factory = newSafeDocumentBuilderFactory();

        factory.setSchema(schema);

        return factory.newDocumentBuilder();
    }

    public static DocumentBuilderFactory newSafeDocumentBuilderFactory() throws ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(
            "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl",
            XmlUtils.class.getClassLoader()
        );

        factory.setValidating(false);
        factory.setIgnoringComments(true);
        factory.setIgnoringElementContentWhitespace(true);
        factory.setNamespaceAware(true);
        factory.setXIncludeAware(false);
        factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        factory.setFeature("http://xml.org/sax/features/external-general-entities", false);
        factory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);

        return factory;
    }

    public static Element only(String selector, Document context) {
        return only(selector, context.getDocumentElement());
    }

    public static Element only(String selector, Element context) {
        return only(null, selector, context);
    }

    public static Element only(String namespace, String selector, Element context) {
        NodeList nodes = namespace != null
            ? context.getElementsByTagNameNS(namespace, selector)
            : context.getElementsByTagName(selector);

        return nodes.getLength() != 1 ? null : (Element)nodes.item(0);
    }

    public static Map<String, Element> map(NodeList nodes, String attribute) {
        Map<String, Element> map = new HashMap<>();

        for (Element element : each(nodes)) {
            map.put(element.getAttribute(attribute), element);
        }

        return map;
    }

    public static Iterable<Element> each(final NodeList nodes) {
        return new Iterable<Element>() {
            public Iterator<Element> iterator() {
                return new ElementIterator(nodes);
            }
        };
    }

    public static Iterable<Node> each(final NamedNodeMap attributes) {
        return new Iterable<Node>() {
            public Iterator<Node> iterator() {
                return new AttributeIterator(attributes);
            }
        };
    }

    public static ElementList every(String selector, Document context) {
        return every(selector, context.getDocumentElement());
    }

    public static ElementList every(String selector, Element context) {
        return every(null, selector, context);
    }

    public static ElementList every(String namespace, String selector, Element context) {
        NodeList nodes = namespace != null
            ? context.getElementsByTagNameNS(namespace, selector)
            : context.getElementsByTagName(selector);

        return new ElementList(nodes);
    }

    private static class AttributeIterator implements Iterator<Node> {
        private final NamedNodeMap nodes;
        private final int length;
        private int index = 0;

        AttributeIterator(NamedNodeMap nodes) {
            this.nodes = nodes;
            this.length = nodes.getLength();
        }

        public boolean hasNext() {
            return this.index < this.length;
        }

        public Node next() {
            return this.nodes.item(this.index++);
        }

        public void remove() {
            throw new UnsupportedOperationException("Cannot remove nodes from this iterator");
        }
    }

    static class ElementIterator implements Iterator<Element> {
        private final NodeList nodes;
        private final int length;
        private int index = 0;
        private Element next;

        ElementIterator(NodeList nodes) {
            this.nodes = nodes;
            this.length = nodes.getLength();
            this.findNext();
        }

        public boolean hasNext() {
            return this.next != null;
        }

        public Element next() {
            Element next = this.next;

            this.findNext();

            return next;
        }

        public void remove() {
            throw new UnsupportedOperationException("Cannot remove nodes from this iterator");
        }

        private void findNext() {
            this.next = null;

            if (this.index == this.length) {
                return;
            }

            Node candidate;

            do {
                candidate = this.nodes.item(this.index++);
            } while (this.index < this.length && !(candidate instanceof Element));

            if (candidate instanceof Element) {
                this.next = (Element)candidate;
            }
        }
    }
}
