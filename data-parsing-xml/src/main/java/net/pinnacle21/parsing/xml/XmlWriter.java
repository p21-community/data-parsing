/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.parsing.xml;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Tim Stone
 */
@SuppressWarnings("unused") // api
public class XmlWriter {
    public static final String SCHEMA_NAMESPACE_URI = "http://www.w3.org/2001/XMLSchema-instance";
    public static final String XLINK_NAMESPACE_URI = "http://www.w3.org/1999/xlink";
    public static final String XML_NAMESPACE_URI = "http://www.w3.org/XML/1998/namespace";

    private static final String BLANK_PREFIX = "";
    private static final String XLINK_PREFIX = "xlink";
    private static final String XML_PREFIX = "xml";

    private final TransformerHandler contentHandler;
    private final String defaultNamespace;
    private final Map<String, String> prefixes = new HashMap<>();

    private final OutputStream targetStream;

    private final Map<String, String> defaultNamespaces = new HashMap<String, String>() {{
        put(XML_NAMESPACE_URI, XML_PREFIX);
        put(XLINK_NAMESPACE_URI, XLINK_PREFIX);
    }};

    public XmlWriter(OutputStream target, String defaultNamespace, Instruction... instructions)
            throws TransformerConfigurationException, SAXException {
        this(target, defaultNamespace, null, instructions);
    }

    public XmlWriter(OutputStream target, String defaultNamespace, Map<String, String> namespaces, Instruction... instructions)
            throws SAXException, TransformerConfigurationException {
        if (namespaces == null) {
            this.defaultNamespaces.put(defaultNamespace, BLANK_PREFIX);
            namespaces = this.defaultNamespaces;
        }

        this.defaultNamespace = defaultNamespace;
        this.targetStream = target;
        Result result = new StreamResult(this.targetStream);
        SAXTransformerFactory factory =
                (SAXTransformerFactory)SAXTransformerFactory.newInstance();
        this.contentHandler = factory.newTransformerHandler();
        Transformer transformer = this.contentHandler.getTransformer();

        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        this.contentHandler.setResult(result);
        this.contentHandler.startDocument();

        if (instructions != null) {
            for (Instruction instruction : instructions) {
                this.contentHandler.processingInstruction(instruction.type,
                        instruction.instruction);
                this.contentHandler.characters(new char[] { '\n' }, 0, 1);
            }
        }

        for (Map.Entry<String, String> namespace : namespaces.entrySet()) {
            this.addMapping(namespace.getKey(), namespace.getValue());
        }
    }

    public void addMapping(String uri, String prefix) throws SAXException {
        this.prefixes.put(uri, prefix);
        this.contentHandler.startPrefixMapping(prefix, uri);
    }

    public void close() throws SAXException, IOException {
        try {
            for (String prefix : this.prefixes.keySet()) {
                this.contentHandler.endPrefixMapping(prefix);
            }

            this.contentHandler.endDocument();
        } finally {
            this.targetStream.close();
        }
    }

    public void characters(String characterData) throws SAXException {
        if (characterData != null) {
            this.contentHandler.characters(characterData.toCharArray(), 0,
                characterData.length());
        }
    }

    public void comment(String...comments) throws SAXException {
        for (String comment : comments) {
            this.contentHandler.comment(comment.toCharArray(), 0, comment.length());
            this.contentHandler.characters(new char[] { '\n' }, 0, 1);
        }
    }

    public void startElement(String localName) throws SAXException {
        this.startElement(this.defaultNamespace, localName);
    }

    public void startElement(String localName, Attributes attributes)
        throws SAXException {
        this.startElement(this.defaultNamespace, localName, attributes);
    }

    public void startElement(String uri, String localName) throws SAXException {
        this.startElement(uri, localName, new AttributesImpl());
    }

    public void startElement(String uri, String localName, Attributes attributes)
        throws SAXException {
        this.contentHandler.startElement(uri, localName, prefix(localName, uri),
            attributes);
    }

    public void simpleElement(String localName, String characterData)
        throws SAXException {
        this.simpleElement(this.defaultNamespace, localName, characterData);
    }

    public void simpleElement(String localName, Attributes attributes)
        throws SAXException {
        this.simpleElement(this.defaultNamespace, localName, attributes, null);
    }

    public void simpleElement(String localName, Attributes attributes,
        String characterData) throws SAXException {
        this.simpleElement(this.defaultNamespace, localName, attributes, characterData);
    }

    public void simpleElement(String uri, String localName, String characterData)
        throws SAXException {
        this.simpleElement(uri, localName, new AttributesImpl(), characterData);
    }

    public void simpleElement(String uri, String localName, Attributes attributes)
        throws SAXException {
        this.simpleElement(uri, localName, attributes, null);
    }

    public void simpleElement(String uri, String localName, Attributes attributes,
        String characterData) throws SAXException {
        this.startElement(uri, localName, attributes);

        if (characterData != null) {
            this.characters(characterData);
        }

        this.endElement(uri, localName);
    }

    public void endElement(String localName) throws SAXException {
        this.endElement(this.defaultNamespace, localName);
    }

    public void endElement(String uri, String localName) throws SAXException {
        this.contentHandler.endElement(uri, localName, prefix(localName, uri));
    }

    public void emptyElement(String localName) throws SAXException {
        this.emptyElement(this.defaultNamespace, localName);
    }

    public void emptyElement(String uri, String localName) throws SAXException {
        this.startElement(uri, localName);
        this.contentHandler.characters(new char[] { ' ' }, 0, 1);
        this.endElement(uri, localName);
    }

    public SimpleAttributes newAttributes() {
        return new SimpleAttributes();
    }

    public SimpleAttributes newAttributes(String name) {
        return new SimpleAttributes(name);
    }

    private String prefix(String localName, String uri) {
        String prefix = BLANK_PREFIX;

        if (this.prefixes.containsKey(uri)) {
            prefix = this.prefixes.get(uri) + ":";
        }

        return prefix + localName;
    }

    public static class Instruction {
        final String type;
        final String instruction;

        public Instruction(String type, String instruction) {
            this.type = type;
            this.instruction = instruction;
        }
    }

    public OutputStream getTargetStream() {
        return targetStream;
    }

    public class SimpleAttributes extends AttributesImpl implements Comparable<SimpleAttributes> {
        private final String name;

        SimpleAttributes() {
            this("");
        }

        SimpleAttributes(String name) {
            this.name = name;
        }

        public SimpleAttributes addEmptyAttribute(String localName) {
            return this.addAttribute(localName, "");
        }

        public SimpleAttributes addEmptyAttribute(String uri, String localName) {
            return this.addAttribute(uri, localName, "");
        }

        public SimpleAttributes addAttribute(String localName, String value) {
            return this.addAttribute(XmlWriter.this.defaultNamespace, localName, value);
        }

        public SimpleAttributes addAttribute(String uri, String localName, String value) {
            if (value == null) {
                value = "";
            }

            super.addAttribute(uri, localName, prefix(localName, uri), "CDATA", value);

            return this;
        }

        public int compareTo(SimpleAttributes attributes) {
            return this.name.compareTo(attributes.name);
        }

    }
}
